﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"=R5F.31QU+!!.-6E.$4%*76Q!!%LA!!!2Z!!!!)!!!%JA!!!!:!!!!!22:97BU?G6F)&amp;2F=X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!+!A!)!!!$!!!!A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!!Y\%F#)PV%GKSYT[0%%LX!!!!!Q!!!!1!!!!!,9?J4'*L6Z"O8+"&amp;@-&gt;J;(5(9T:DQ#S"/G!#:DM_%*_!!!!!!!!!!!HCJ0DZ7_^3,PJ0019I26S!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%*W&amp;.GZ]1?L`*7-+:3QOE.Q!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!H!!!!*(C=9_"E9'JAO-!!R)R!T.4!^!0)`A$C-QBQA%A'"A#R%AGB!!!!!%=!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!N)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=QC]AQ(13C%_AG]/)R7Q!?.UI&amp;!!!!!!-!!&amp;73524!!!!!!!$!!!"&lt;A!!!NRYH%NA:'$).,9Q/Q#EG9&amp;9A;'")4E`*:7,!=BHA)!D4!Q5AQ#I?6JIYI9($K="A2[`@!O9X_WCQN*=I],$6-LXPU3&amp;)_!&amp;3,$Z#-@B&lt;I_=YYYW9#5=71R:$!(`!T/;D`#!&gt;30L^V&amp;B-4T15+H-5#J]P.'%%7*,).19FM.!]Q7!;HC[1V29/LP%A%\K&lt;*)#EAW7$0R4$T:9!-E$"C&gt;-'0GX(4D-,_]!&gt;A^10=@"BSS^%]%7&gt;),)TB"'C?-OD$JC1(9P$W-AQJVI\AHL"PICCT%-*N`.&gt;NR"!]1_\C!#I4)A6!7%+A"2/U"%X'&amp;CQH8N[XO\1/(+BC4G!-5.1!S+6RD79W"E!!5)%R##&lt;0XT``^`'[!)%V2-%3I'9J_%MD71^,S%CDEAW103#T*B,:$7A,)X1^E.5(?"R,Y#[1)I_Q]$*$W!W+!UNQ$+ZA+S";"M13$\!:1N"G5@A-9G/OXM\_++(#;Q^!I!_G&gt;V6!!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A(%9DY"\&gt;^`T_TH@]`N_X`0\%&gt;`T````D!!!!9Q!!!'-"!!"A!P!!9!3'!'!)A/"A%)"A9##!Y'"AA;"A@)'A9(_$)'"`^C"A@`QA9(`])'"``S"A@`XA9(`]1'"``)"A@`W!9$`Z!'!(_A"A!(Q!9!!)!'!!!!"`````Q!!!A$`````````````````````]!!!!!``!!]!$`!!]!$```!!!!!0``$`$`]0``]0`````Q$````Q`Q$`]!``$`````]!````]0]0```Q`Q``````!0````$`!!]!$`]0`````Q$`````````````````]!!C!!!!!!!!!!!!!!!!$`!!)A!!!!!!!!!!!!!!!!`Q!#)!!!$#T!!!!!!!!!!0]!!!!!!-ZG:CT!!!!!!!$`!!!!!!QG:G:G9MQ!!!!!`Q!!!!$#:G:G:G:G\1!!!0]!!!!!*G:G:G:G:G)!!!$`!!!!$G:G:G:G:G&lt;G!!!!`Q!!!/:G:G:G:G:O:A!!!0]!!!$O\G:G:G:GZG9!!!$`!!!!ZG:OZG:G&lt;G:G!!!!`Q!!!#:G:G&lt;O:O:G:A!!!0]!!!!G:G:G:G*G:G9!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:M!!!!`Q!!!#:G:G:G&lt;G:GQ!!!!0]!!!"G:G:G:GZG9A!!!!$`!!!!T?:G:G:O:C!!!!!!`Q!!!!!-XG:G&lt;G,!!!!!!0]!!!!!!!$.ZGZM!!!!!!$`!!!!!!!!!!T3Q!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````]E*#1E*#1E*0```Q!!!0]!!!$``Q!!!0]!!!$``````S1E*#1E*#1E`````Q$``Q$```]!``````]!````````````*#4`````````!0``!!$```]!!0```Q$```````````]E*0````````]!``]!````````!0``!0```````````S1E`````````Q$``Q!!!0]!!!$```]!````````````*#4```````````````````````````````````]!!!$4UQ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!.04!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!U^-!!!!!!!$WIXHY!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!^M&gt;YIJT'H68W!!!!!!!!!!!!!!$``Q!!!!!!!!!!!0;D?(CC?(BY?-;D?3M!!!!!!!!!!0``!!!!!!!!!!$WIXBY?+*Y?(BY?(BYH-?!!!!!!!!!``]!!!!!!!!!!+.Y?(BYIHBY?(BY?(BYH-]!!!!!!!$``Q!!!!!!!!#E?(BY?(CC?(BY?(BY?*T(RA!!!!!!!0``!!!!!!!!J:RY?(BY?+*Y?(BY?(C=RXD'!!!!!!!!``]!!!!!!!$(R]@(H(BYIHBY?(BY?-?=?-9!!!!!!!$``Q!!!!!!!-?=H*T'R]@'?(BY?(D(H(BYRA!!!!!!!0``!!!!!!!!IZS=H*S=H-&lt;(R]:YRZRY?(D'!!!!!!!!``]!!!!!!!#DH*S=H*SCH*S=RM[=?(BY?-9!!!!!!!$``Q!!!!!!!+/=H*S=IJS=H*S=RZRY?(BYIQ!!!!!!!0``!!!!!!!!IZS=H++=H*S=H*T(IK+C?(CD!!!!!!!!``]!!!!!!!#DH*SCH*S=H*S=H-?=?++CIK-!!!!!!!$``Q!!!!!!!+/=IJS=H*S=H*S=RZRY?(C=?1!!!!!!!0``!!!!!!!!I[+=H*S=H*S=H*T(H(BYH(E!!!!!!!!!``]!!!!!!!#CH*S=H*S=H*S=H-?=?*SD!!!!!!!!!!$``Q!!!!!!!0C!R[+=H*S=H*S=RXBYIQ!!!!!!!!!!!0``!!!!!!!!!!!!_)$(RJS=H*T(?+0W!!!!!!!!!!!!``]!!!!!!!!!!!!!!!$Y?M@'H-@'+Q!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!#N[KSM!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!!1!!!!!!!!$?Q!!#.2YH+V7/UR452D_4SHG&amp;&amp;&amp;/A@)QV$Y]299W+III&amp;C&amp;S"8GLC!%(N,%I'').$]-%SZ5%%S93"B-G%V9("B='9RI(OT!ZC%H$Q-Z#.*(&lt;[X`OJ&lt;WXD\1;\0#H;&lt;\P@XX@_6-!OMVK,!F95)#Q!`T3JU"*/%9!IA%+2Z`[*7$$Z$?1SFKCQ#!&gt;:DO7"+F4I$1=K[=8J7890U+4*EMLWU8I#6;,S5I5+!P(KOU^0-\YBTK_8*T-;A=H7S%*SQBX`;1L=A1,ANQAIDV!%E!ED^5K?__'8I\,80RK#^";,;6.!3&lt;&amp;H.-]@BYT9OH07EK,HX!M@:13-'5$&lt;'ZO'C3\4KL8WLC'(-+V&gt;PVZ//8)G?&amp;RP]9JU4B9JSF:2X)F6E8PAJ2*L:"C$+H)GU_OBC&gt;L33[F03&gt;P1I&amp;+(L^&amp;;_FO_(PRK8PS2S"!II_J_F6^)`DW!;'#BCV()8QNB!XB^S%&amp;,MERSR/QCFI6Q'!-XGMS7*-S8%9:3+=GAUV;:G$IM&amp;J!"]E;^4I\JO:G:M?HX:&amp;H\K&gt;4I:E:^[PJS&gt;?BW8&amp;X/$1&lt;UB6C0LUX:!&amp;P)$C%TST;&amp;3FG;R9,%@6VPTD!#FX1&lt;B9A!BM&lt;'\A4D!;V#;H60*\CW&lt;&amp;;,UAJ%=,'-E5DRD+PYD*^I_"\".+P"]`&amp;7HUNFH1T.RNGPIZGNL0-ZN,-@/0`G,H!KI*IOY5-@U-1'K%`DV&gt;&lt;&gt;:,B&lt;]1$&gt;%!Q$_=G=B&lt;.`E:/%$H^B@X&gt;FO6PQ=XQ^^L;7K;`WV/3[#[X%K+\8.Z7$^6$Y@5^&gt;1]O1*P*[Y/I%V:2@[D\="L][:JU#G&gt;X;:IY$'=8ISC_AM[7P1/2X+*9UE1J6;!,4&gt;D)YQ_V+G6;&amp;3?=AQ:IR%(0^G*K%?UO`&gt;'22*%\[N,X=B,+:,=']9D)P%?\@5'^)HUW.+Q++-&lt;#U/.HN&lt;G-'?`AD&amp;5]\EC_A2J=9Q#;==#NL3U=%'/O!2WY!\=']9C9SD_:X9I/&amp;&lt;\QC&amp;A9?PSMZA'\^3-U;B*2T)D/.&lt;M]VYRH=!VO$?)2-66#SOYG*V3]P\^$`HN3]Y1^_I1DJAEL^,?:.3&amp;*L_"#3+ZG*N*+Y"0O.6X6_T&lt;^KB&lt;B66U;%U)A&gt;M"U=OD/F\H5`QA[R#5V(Y$&gt;RB+,#DCEDP*0L&amp;MU3&lt;PJ&lt;&lt;;/&amp;RK0&gt;26&gt;JQ@2&lt;[F`'N'_&amp;&amp;0?BL&gt;EHL^TU:I`,T*20Q!!!!!%!!!!+1!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!!1!!!!!!!!!:1!!!(6YH'.A9#A5E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````6SFCZ0B[Z"J=U2%@/&amp;.FFDS("!"F#"G;!!!!!!!!"!!!!!=!!!*U!!!!#!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"))!#!!!!!!!%!#!!Q`````Q!"!!!!!!!M!!!!!A!+1#%%4G^O:1!!'E"1!!%!!""5:7VQ&lt;'&amp;U:62F=X2$98.F!!!"!!%!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"EA!)!!!!!!!1!&amp;!!=!!!%!!.K(/"5!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!WI=Y&amp;1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"))!#!!!!!!!%!#!!Q`````Q!"!!!!!!!M!!!!!A!+1#%%4G^O:1!!'E"1!!%!!""5:7VQ&lt;'&amp;U:62F=X2$98.F!!!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"EA!)!!!!!!!1!&amp;!!-!!!%!!!!!!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!.3!!A!!!!!!#!!J!)12/&lt;WZF!!!;1&amp;!!!1!!%&amp;2F&lt;8"M982F6'6T&gt;%.B=W5!!!%!!1!!!!!!!!!!&amp;%Z*,ER7,E&amp;M&lt;#Z4&lt;X6S9W60&lt;GRZ!!!!&amp;3!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#EA!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!%!!!!!!!!!!!!!!!1!!Q!,!!!!"!!!!%]!!!!I!!!!!A!!"!!!!!!M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!09!!!&amp;`?*S.D\V/QU!1B$`\4/+9`"BQ[*!O$15&amp;$5V+3UAJ591II/.EHQ(JB&amp;&amp;]C2!6(?`)5]!&lt;M('#%+*"I^O@W&gt;(M(H")EE_CC`L2QD7@\W`4-2"G._&lt;?PVCLLWTD4^WK=+:JUF^EY2UH_:Q!DM\&gt;MP&amp;WI?N+NUL^N(B9'7^V;&lt;RB2U"##=%(I42KWX*'2#`PICJX2ZS`KPL3&lt;QB&lt;6[4CLOD1*6&lt;,MB,NB([\-6(_O230O&gt;DVW/8YHX?%3/CMA[BD&lt;O7%C*H9;);-:,R'+NQXQCX_-D_4P2;&lt;$Q8M3V7UPI%]'%B7ML!PV1':-"FDK1@Q"4N(/:Q!!!!!!)E!!1!#!!-!"A!!!'A!$Q1!!!!!$Q$:!.1!!!"R!!]%!!!!!!]!W1$5!!!!?A!0"!!!!!!0!.E!V!!!!)/!!)1!A!!!$Q$:!.1!!!#&amp;A!#%!)!!!!]!W1$5!!!!BY!!B!#!!!!0!.E!V!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%S!4!"-1!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"+Y!!!%?1!!!#!!!"+9!!!!!!!!!!!!!!!A!!!!.!!!"'A!!!!?4%F#4A!!!!!!!!&amp;Y4&amp;:45A!!!!!!!!'-5F242Q!!!!!!!!'A1U.46!!!!!!!!!'U4%FW;1!!!!!!!!()1U^/5!!!!!!!!!(=6%UY-!!!!!!!!!(Q2%:%5Q!!!!!!!!)%4%FE=Q!!!!!!!!)96EF$2!!!!!!!!!)M&gt;G6S=Q!!!!1!!!*!5U.45A!!!!!!!!+E2U.15A!!!!!!!!+Y35.04A!!!!!!!!,-;7.M.!!!!!!!!!,A;7.M/!!!!!!!!!,U4%FG=!!!!!!!!!-)2F"&amp;?!!!!!!!!!-=2F")9A!!!!!!!!-Q2F"421!!!!!!!!.%6F"%5!!!!!!!!!.94%FC:!!!!!!!!!.M1E2&amp;?!!!!!!!!!/!1E2)9A!!!!!!!!/51E2421!!!!!!!!/I6EF55Q!!!!!!!!/]2&amp;2)5!!!!!!!!!01466*2!!!!!!!!!0E3%F46!!!!!!!!!0Y6E.55!!!!!!!!!1-2F2"1A!!!!!!!!1A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!1Q!!!!!!!!!!0````]!!!!!!!!"&amp;!!!!!!!!!!!`````Q!!!!!!!!&amp;!!!!!!!!!!!$`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!%`````Q!!!!!!!!-1!!!!!!!!!!@`````!!!!!!!!!S1!!!!!!!!!#0````]!!!!!!!!$.!!!!!!!!!!*`````Q!!!!!!!!.)!!!!!!!!!!L`````!!!!!!!!!VA!!!!!!!!!!0````]!!!!!!!!$&lt;!!!!!!!!!!!`````Q!!!!!!!!/%!!!!!!!!!!$`````!!!!!!!!!ZA!!!!!!!!!!0````]!!!!!!!!%(!!!!!!!!!!!`````Q!!!!!!!!9A!!!!!!!!!!$`````!!!!!!!!#C1!!!!!!!!!!0````]!!!!!!!!+.!!!!!!!!!!!`````Q!!!!!!!!I]!!!!!!!!!!$`````!!!!!!!!$&lt;Q!!!!!!!!!!0````]!!!!!!!!.R!!!!!!!!!!!`````Q!!!!!!!!X-!!!!!!!!!!$`````!!!!!!!!$&gt;Q!!!!!!!!!!0````]!!!!!!!!.Z!!!!!!!!!!!`````Q!!!!!!!!Z1!!!!!!!!!!$`````!!!!!!!!$FA!!!!!!!!!!0````]!!!!!!!!1U!!!!!!!!!!!`````Q!!!!!!!"$9!!!!!!!!!!$`````!!!!!!!!%/!!!!!!!!!!!0````]!!!!!!!!2$!!!!!!!!!#!`````Q!!!!!!!"))!!!!!"":97BU?G6F)&amp;2F=X1O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!22:97BU?G6F)&amp;2F=X1O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!1!!1!!!!!!!!%!!!!#!!J!)12/&lt;WZF!!!)!&amp;!!!1!!!!%!!1!!!!!!!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!))9!#!!!!!!!!!!!!!!%!!!!!!!%"!!!!!1"?!0(;7G3$!!!!!BB5:7VQ&lt;'&amp;U:62F=X2$98.F,GRW9WRB=X-56'6N='RB&gt;'65:8.U1W&amp;T:3ZD&gt;'Q!+%"1!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!!!!!!!!!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!1!!!!!!!A%!!!!#!!J!)12/&lt;WZF!!"A!0(;BTA6!!!!!BB5:7VQ&lt;'&amp;U:62F=X2$98.F,GRW9WRB=X-56'6N='RB&gt;'65:8.U1W&amp;T:3ZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!#!!J!)12/&lt;WZF!!"A!0(;BTA6!!!!!BB5:7VQ&lt;'&amp;U:62F=X2$98.F,GRW9WRB=X-56'6N='RB&gt;'65:8.U1W&amp;T:3ZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````A!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!#!!A!!!!!!!!!!!!!!"!!!!'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Parent Libraries" Type="Parent Libraries">
		<Item Name="TestCase.lvclass" Type="Parent" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/VI Tester/TestCase.llb/TestCase.lvclass"/>
	</Item>
	<Item Name="Yahtzee Test.ctl" Type="Class Private Data" URL="Yahtzee Test.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1352671760</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
	<Item Name="testExample.vit" Type="VI" URL="../testExample.vit">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="test_chance.vi" Type="VI" URL="../test_chance.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="test_Yahtzee_scores_50 points.vi" Type="VI" URL="../test_Yahtzee_scores_50 points.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
	</Item>
	<Item Name="test_ones.vi" Type="VI" URL="../test_ones.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
	</Item>
	<Item Name="test_twos.vi" Type="VI" URL="../test_twos.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_threes.vi" Type="VI" URL="../test_threes.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_force.vi" Type="VI" URL="../test_force.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_add up fives.vi" Type="VI" URL="../test_add up fives.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_add_all_sixes.vi" Type="VI" URL="../test_add_all_sixes.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_three ofa kind.vi" Type="VI" URL="../test_three ofa kind.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_four kind.vi" Type="VI" URL="../test_four kind.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_FullHouse.vi" Type="VI" URL="../test_FullHouse.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_Hi_strt.vi" Type="VI" URL="../test_Hi_strt.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test_Low_Straight.vi" Type="VI" URL="../test_Low_Straight.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;FB;(2[:75A6'6T&gt;#ZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
