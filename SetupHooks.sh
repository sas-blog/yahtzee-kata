#! /bin/bash
# set error handling
set -euo pipefail
echo "Setting up hooks"
echo "Enter V for VI Tester, C for Caraya"
read -r choice
if [[ $choice == "V" ]]; then
	cp ./Hook\ Templates/VIT-PreCommit.sh ./.git/hooks/pre-commit
fi
if [[ $choice == "C" ]]; then
	cp ./Hook\ Templates/Caraya-PreCommit.sh ./.git/hooks/pre-commit
fi
