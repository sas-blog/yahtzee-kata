#! /bin/bash
# set error handling
set -euo pipefail
#Mark start time
T="$(date +%s)"
#set constants
# die is error handling catch function. In this case removed exit LabVIEW.
die() { echo "FAILED: "$1 >&2; exit 1;}
#make sure g-cli is in path
PATH=$PATH:/c/Program\ Files/G-CLI
g-cli vitester -- -r VITester.xml VITester.lvproj die "Test Failed with "$?" Results in VITester.xml"
echo "=========="
echo "PASSED"
T="$(($(date +%s)-T))"
echo "Total Time: ${T}s"

